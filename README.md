1\. domaća zadaća iz analize i projektiranja računalom
=======

U ovoj domaćoj zadaći bilo je potrebno programski ostvariti vlastite klase za matrice, implementirati LU i LUP dekompoziciju matrice, rješavač linearnih sustava pomoću LUP dekompozicije, unaprijedne i unatražne supstitucije i metodu za inverziju matrica.


To je ostvareno u programskom jeziku java.

Tekst zadatka se nalazi [ovdje](https://www.fer.unizg.hr/_download/repository/vjezba_1%5B5%5D.pdf).
