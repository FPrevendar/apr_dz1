package glavni;

import matrix.IMatrix;
import matrix.Matrix;

public class CetvrtiZadatak {

	public static void main(String[] args) {
		System.out.println("Cetvrti zadatak");
		IMatrix m = Matrix.parseMatrix("0.000001 3000000 2000000\n"
				+ "1000000 2000000 3000000\n"
				+ "2000000 1000000 2000000");
		IMatrix vektor = Matrix.parseMatrix("12000000.000001\n"
				+ "14000000\n"
				+ "10000000");
		
		Matrix.setEps(10e-10);// treba ovo postaviti, inace se ne moze dekomponirati s LU
		// 10e-6 primjeti  blizinu nuli
		System.out.println("Matrica:");
		m.print();
		System.out.println("Vektor:");
		vektor.print();
		
		System.out.println("Sa LU dekompozicijom");
		Matrix.rjesiSustavLUdekompozicijom(m, vektor).print();
		
		System.out.println("Sa LUP dekompozicijom");
		Matrix.rjesiSustavLUPdekompozicijom(m, vektor).print();
		
		//razlika je u tome sto LU ima prvi stozerni element blizak nuli pa se akumulira greska
		Matrix.setEps(10e-6); // vratim default vrijednost
	}
}
