package glavni;

import matrix.IMatrix;
import matrix.Matrix;

public class DestiZadatak {

	public static void main(String[] args) {
		System.out.println("Deseti zadatak");
		IMatrix mat = Matrix.parseMatrix("3 9 6\n 4 12 12\n 1 -1 1");
		System.out.println("Matrica:");
		mat.print();
		System.out.println("Determinanta je: " + mat.determinanta());
	}

}
