package glavni;

import matrix.IMatrix;
import matrix.Matrix;

public class DevetiZadatak {

	public static void main(String[] args) {
		System.out.println("Deveti zadatak");
		IMatrix mat = Matrix.parseMatrix("4 -5 -2\n 5 -6 -2\n-8 9 3");
		System.out.println("Matrica:");
		mat.print();
		System.out.println("Determinanta je : " + mat.determinanta());
	}

}
