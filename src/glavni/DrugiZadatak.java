package glavni;

import matrix.IMatrix;
import matrix.Matrix;

public class DrugiZadatak {

	public static void main(String[] args) {
		System.out.println("Drugi zadatak");
		IMatrix matrica = Matrix.parseMatrix("3 9 6\n 4 12 12\n1 -1 1");
		IMatrix vektor = Matrix.parseMatrix("12\n12\n1");
		System.out.println("Matrica:");
		matrica.print();
		System.out.println("Vektor:");
		vektor.print();
		
		try{
			Matrix.rjesiSustavLUdekompozicijom(matrica, vektor).print();
		}catch(Exception e) {
			System.out.println("Nije rjesivo s LU, rjesavam s LUP");
			System.out.println("Rjesenje:");
			Matrix.rjesiSustavLUPdekompozicijom(matrica, vektor).print();
		}
		//nije rjesivo s LU jer se dobije 0 kao stozerni element kod LU dekompozicije
	}

}
