package glavni;

/**
 * @author filip
 *
 */
public class Glavni {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		PrviZadatak.main(null);
		DrugiZadatak.main(null);
		TreciZadatak.main(null);
		CetvrtiZadatak.main(null);
		PetiZadatak.main(null);
		SestiZadatak.main(null);
		SedmiZadatak.main(null);
		OsmiZadatak.main(null);
		DevetiZadatak.main(null);
		DestiZadatak.main(null);
		
	}

}
