package glavni;

import matrix.IMatrix;
import matrix.Matrix;

public class PetiZadatak {

	public static void main(String[] args) {
		System.out.println("Peti zadatak");
		IMatrix mat = Matrix.parseMatrix("0 1 2\n 2 0 3\n 3 5 1");
		IMatrix vec = Matrix.parseMatrix("6\n 9\n3");
		System.out.println("Matrica:");
		mat.print();
		System.out.println("Vektor");
		vec.print();
		System.out.println("Rjesenje:");
		try {
			Matrix.rjesiSustavLUdekompozicijom(mat, vec).print();
		}catch(Exception e) {
			Matrix.rjesiSustavLUPdekompozicijom(mat, vec).print();
		}
		// na papiru se dobije rjesenje
		//[0]
		//[0]
		//[3]
		// sto je jako blizu ovome,  greska ovdje dolazi zbog toga sto 
		// double brojevi ne mogu savrseno prikazati svaki decimalni (pa cak niti racionali)
		// broj

	}

}
