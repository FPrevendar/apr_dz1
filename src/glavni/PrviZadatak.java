package glavni;

import matrix.IMatrix;
import matrix.Matrix;

public class PrviZadatak {

	public static void main(String[] args) {
		System.out.println("Prvi zadatak");
		// neka radnom matrica
		IMatrix m = Matrix.parseMatrix("1.10000001 2.3 454.56\n 34e-5 322.4 45.6\n 46.67 3.245 454.544");
		IMatrix m2 = m.copy();
		m2.div(0.3);
		m2.mul(0.3);
		System.out.println("Matrica 1:");
		m.print();
		System.out.println("Matrica 2:");
		m2.print();
		System.out.println(m.compare(m2) ? "iste su" :"drugacije su");
	}

}
