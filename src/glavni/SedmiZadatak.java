package glavni;

import matrix.IMatrix;
import matrix.Matrix;

public class SedmiZadatak {

	public static void main(String[] args) {
		System.out.println("Sedmi zadatak");
		IMatrix mat = Matrix.parseMatrix("1 2 3\n 4 5 6\n 7 8 9");
		System.out.println("Matrica:");
		mat.print();
		
		try {
			mat.inverz().print();
		}catch(Exception e) {
			System.out.println("Problem s djeljenjem s nulom pri eps = 10e-6");
		}
		System.out.println("LU matrica kod LUP dekompzicije");
		mat.LUPdekompozicija().LU.print();
		//problem je nula na zadnjem elementu (10e-16)
		// sto daje djeljenje s nulom pri supstituciji unatrag
		
		Matrix.setEps(10e-20);
		System.out.println("Uz manji eps (10e-20)");
		mat.inverz().print();
		
		Matrix.setEps(10e-6);//vratim na pocetno
		
	}

}
