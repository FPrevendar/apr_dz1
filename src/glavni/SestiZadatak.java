package glavni;

import matrix.IMatrix;
import matrix.Matrix;

public class SestiZadatak {

	public static void main(String[] args) {
		System.out.println("Sesti zadatak");
		IMatrix mat = Matrix.parseMatrix(
				  "4000000000 1000000000 3000000000\n"
				+ "4 2 7\n"
				+ "0.0000000003 0.0000000005 0.0000000002");
		IMatrix vec = Matrix.parseMatrix("9000000000\n 15\n 0.0000000015");
		// zadana vrijednost za eps mi je 10-6
		System.out.println("Matrica:");
		mat.print();
		System.out.println("Vektor");
		vec.print();
		
		try {
			IMatrix rjesenje = Matrix.rjesiSustavLUPdekompozicijom(mat, vec);
			rjesenje.print();
		}catch(Exception e) {
			System.out.println("Nije rjesivo u ovom obliku");
		}
		
		// s povecanjem epsilona
		Matrix.setEps(10e-20);
		System.out.println("Uz promjenu tolerancije na 10e-20:");
		try {
			IMatrix rjesenje = Matrix.rjesiSustavLUPdekompozicijom(mat, vec);
			rjesenje.print();
		}catch(Exception e) {
			System.out.println("Nije rjesivo u ovom obliku");
		}
		
		Matrix.setEps(10e-6);
		// s obzirom na to da je ovo sustav jednadzbi, mozemo slobodno redove mnoziti bilo
		// kojim skalarom
		// pa cu prvi red podijeliti s 10e8 i zadnji pomnoziti s 10e9
		System.out.println("Nakon transformacije");
		mat.divElem(0, 0, 10e8);
		mat.divElem(0, 1, 10e8);
		mat.divElem(0, 2, 10e8);
		vec.divElem(0, 0, 10e8);
		
		mat.mulElem(2, 0, 10e9);
		mat.mulElem(2, 1, 10e9);
		mat.mulElem(2, 2, 10e9);
		vec.mulElem(2, 0, 10e9);
		
		System.out.println("Matrica:");
		mat.print();
		System.out.println("Vektor:");
		vec.print();
		
		try {
			IMatrix rjesenje = Matrix.rjesiSustavLUPdekompozicijom(mat, vec);
			System.out.println("Rjesenje:");
			rjesenje.print();
		}catch(Exception e) {
			System.out.println("Nije rjesivo u ovom obliku");
		}
		
	}

}
