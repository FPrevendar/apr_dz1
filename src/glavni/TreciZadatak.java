package glavni;

import matrix.IMatrix;
import matrix.IMatrix.LUPRjesenje;
import matrix.Matrix;

public class TreciZadatak {

	public static void main(String[] args) {
		System.out.println("Treci zadatak");
		// TODO Auto-generated method stub
		IMatrix m = Matrix.parseMatrix("1 2 3\n 4 5 6\n 7 8 9");
		System.out.println("Originalna matrica");
		m.print();
		System.out.println();
		IMatrix zaLU = m.copy();
		IMatrix zaLUP = m.copy();
		zaLU.LUdekompozicija();
		System.out.println("LU dekomponirana matrica");
		zaLU.print();
		System.out.println();
		
		LUPRjesenje rj = zaLUP.LUPdekompozicija();
		System.out.println("LUP dekomponirana");
		rj.LU.print();
		System.out.println("P matrica");
		rj.P.print();
		
		
		// sustav nije rjesiv jer se dobije da je zadnji element matrice 0(ili blizak nuli), sto
		// dovodi do dijeljenja s nulom pri suptituciji unatrag kod bilo kojeg ulaznog vektora
	}

}
